const { Pool } = require('pg');
const pgConfig = require("../config/switchdbconfig");
const pool = new Pool(pgConfig);
module.exports = pool;