const express = require('express');

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

var gettripdetails = require('./route/gettripdetails');
app.use("/gettripdetails", gettripdetails);


const port = process.env.PORT || 8002;
app.listen(port, () => { console.log(`Server running on ${port}...`) });