const express = require('express');
const router = express.Router();
var response = require("../commonmethods/response");
const pgpool = require('../db/dbconfig');


router.get('/',async (req,res)=>{
    try{
        //console.log(req);
        const driverId = req.query.driver_id != undefined ? req.query.driver_id : 0;
        const policyId = req.query.policy_id != undefined ? req.query.policy_id : 0;
        const runningPolicyNumber = req.query.running_policy_number != undefined ? req.query.running_policy_number : 0;
        const startDate = req.query.start_date;
        const endDate = req.query.end_date != undefined ? req.query.end_date : 0;
        let selectquery ;
        if(endDate !=0){
            if(driverId != 0){
                selectquery = `select * from trip_details where driver_id = ${driverId} and autowiz_starttime >= ${startDate}`;
                console.log(selectquery);
            }else if(policyId != 0){
                selectquery = `select * from trip_details where autowiz_policy_no = ${policyId} and autowiz_starttime >= ${startDate}`;   
            }else if(runningPolicyNumber != 0){
                let policyIdQuery = `select policy_id from policy_details where running_policy_number = ${runningPolicyNumber}`
                await pgpool.query(policyIdQuery,(err,resp)=>{
                    if(!err){
                        console.log("resp "+resp);
                    }
                   
                });
                //console.log('policyidData '+policyIdData)
               // selectquery = `select * from trip_details where autowiz_policy_no = ${policyIdData} and autowiz_starttime >= ${startDate}`;   
            }
        }else{
            if(driverId != 0){
                selectquery = `select * from trip_details where driver_id = ${driverId} and autowiz_starttime >= ${startDate} and autowiz_endtime <=${endDate}`;
                console.log(selectquery);
            }else if(policyId != 0){
                selectquery = `select * from trip_details where autowiz_policy_no = ${policyId} and autowiz_starttime >= ${startDate} and autowiz_endtime <=${endDate}`;   
            }else if(runningPolicyNumber != 0){
                let policyIdQuery = `select policy_id from policy_details where running_policy_number = ${runningPolicyNumber}`
                
                 await pgpool.query(policyIdQuery,(err,resp)=>{
                    console.log(resp);
                });
               // console.log('policyidData '+policyIdData)
                //selectquery = `select * from trip_details where autowiz_policy_no = ${policyIdData} and autowiz_starttime >= ${startDate} and autowiz_endtime <=${endDate}`;   
            }
        }
        // pgpool.query(selectquery,(err,resp)=>{
        //     if(!err){
        //         //console.log(resp);
        //         if(resp.rowCount >0){
        //             res.send(response.successErrorResponse(200,true,"data fetch successfully",resp.rows));
                    
        //         }else{
        //             res.send(response.successErrorResponse(200,true,"data not found"));
        //         }
        //     }else{
        //         console.log('err '+err);
        //         res.send(response.successErrorResponse(500,false,'Internal server error'));
        //     }
        // })
    }catch(error){
        console.log('in catch '+ error);
        res.send(response.successErrorResponse(500,false,'Internal server error'));
    }
});


module.exports = router;